﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetCreator : MonoBehaviour
{
    [SerializeField] private Planet _template;
    [SerializeField] private float _delay;
    [SerializeField] private PlanetController _planetController;

    private float _timeLeft = 0;

    private void Start()
    {
        if(_template == null)
        {
            Debug.Log("Template not added");
            gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        _timeLeft += Time.deltaTime;

        if(_timeLeft >= _delay)
        {
            Vector3 randPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height)));
            CreatePlanet(1f, randPosition);

            _timeLeft = 0;
        }
    }

    public Planet CreatePlanet(float mass, Vector3 position, bool withDisabledCollider = false)
    {
        Planet newPlanet = Instantiate(_template, position, Quaternion.identity);
        newPlanet.Init(mass, _planetController.Planets, withDisabledCollider ? 0.5f : 0f);
        _planetController.AddPlanet(newPlanet);

        return newPlanet;
    }

    public void Explosion(Vector3 position)
    {
        Planet planet = CreatePlanet(1f, position);
        planet.AddGravityForce(new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), 0));
    }
}
