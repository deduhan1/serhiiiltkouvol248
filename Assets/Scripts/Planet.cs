﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Planet : MonoBehaviour
{
    [SerializeField] private float _speed = 1f;
    [SerializeField] private float _windResistance = 0.05f;

    private Rigidbody2D _rigidbody;
    private CircleCollider2D _collider2D;
    private SpriteRenderer _spriteRenderer;
    private List<Planet> _planets;

    public float Mass => _rigidbody.mass;

    public UnityEvent<Planet> impact;

    private void Awake()
    {
        _collider2D = GetComponent<CircleCollider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Init(float mass, List<Planet> planets, float turnOnColliderWithDelay = 0f)
    {
        _rigidbody.mass = mass;
        _planets = planets;

        transform.localScale = transform.localScale * 2 * Mathf.Sqrt(mass / Mathf.PI);

        StartCoroutine(ColliderOn(turnOnColliderWithDelay));
    }

    private void FixedUpdate()
    {
        foreach (Planet planet in _planets)
        {
            if (planet == this)
                continue;

            Vector3 forceDirection = (transform.position - planet.transform.position).normalized;
            float distanceSqr = (transform.position - planet.transform.position).sqrMagnitude;
            float strength = _speed * _rigidbody.mass * _rigidbody.mass / distanceSqr;

            planet.AddGravityForce(forceDirection * strength);
        }

        _rigidbody.AddForce(_rigidbody.velocity * _windResistance * -1f);
    }

    public void AddGravityForce(Vector3 direction)
    {
        _rigidbody.AddForce(direction, ForceMode2D.Force);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        impact.Invoke(this);
    }

    private void ColliderOff()
    {
        _collider2D.enabled = false;

        StartCoroutine(ColliderOn(0.5f));
    }

    private IEnumerator ColliderOn(float _delay = 0f)
    {
        yield return new WaitForSeconds(_delay);

        _collider2D.enabled = true;
    }


}
