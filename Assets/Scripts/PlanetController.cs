﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour
{
    public enum State
    {
        Creating,
        Repulsion
    }

    [SerializeField] private int _maxPlanetCount;
    [SerializeField] private PlanetCreator _planetCreator;

    private int _planetsCreated = 0;
    private List<Planet> _planets;
    private State _state;
    private Planet _impactedPlanet = null;

    public List<Planet> Planets => _planets;

    private void Start()
    {
        _planets = new List<Planet>(_maxPlanetCount);

        StartCoroutine(Creating());
    }

    private IEnumerator Creating()
    {
        _state = State.Creating;

        while(_state == State.Creating)
        {
            yield return null;
        }

        StartCoroutine(Repulsion());
    }

    private IEnumerator Repulsion()
    {
        _state = State.Repulsion;
        _planetCreator.enabled = false;

        while (_state == State.Repulsion)
        {
            yield return null;
        }

    }

    public void AddPlanet(Planet planet)
    {
        _planetsCreated++;
        _planets.Add(planet);
        planet.impact.AddListener(AddPlanetToCombine);

        if(_planetsCreated == 250)
        {
            _state = State.Repulsion;
        }
    }

    private void AddPlanetToCombine(Planet planet)
    {
        if (_impactedPlanet != null)
        {
            planet.gameObject.SetActive(false);
            _impactedPlanet.gameObject.SetActive(false);

            Combine(planet, _impactedPlanet);

            Destroy(planet.gameObject);
            Destroy(_impactedPlanet.gameObject);

            _planets.Remove(planet);
            _planets.Remove(_impactedPlanet);

            _impactedPlanet = null;
        }
        else
        {
            _impactedPlanet = planet;
        }
    }

    public void Combine(Planet firstPlanet, Planet secondPlanet)
    {
        float mass = firstPlanet.Mass + secondPlanet.Mass;
        Vector3 position = (firstPlanet.transform.position + secondPlanet.transform.position) / 2;

        if (mass < 50)
        {
            _planetCreator.CreatePlanet(mass, position);
        }
        else
        {
            for(int i = 0; i < mass; i++)
            {
                _planetCreator.Explosion(position);
            }
        }
    }
}
